#!/usr/bin/env python
# -*- coding: utf-8 -*-


import wx
import time
import wx.lib.inspection


SECONDS_TO_WAIT = 5


class MyForm(wx.Frame):

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title, size=(300, 300))

        # Add a panel so it looks correct on all platforms
        self.panel = wx.Panel(self, wx.ID_ANY)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.panel.SetSizer(self.sizer)

        # create a textfield with a countdown
        self.countdown = wx.StaticText(self.panel, label="")
        self.sizer.Add(self.countdown, 0, wx.ALIGN_CENTER_VERTICAL)

        # init timer and counter
        self.counter = 0

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.cycle, self.timer)

        # the timer event gets fired every second
        self.timer.Start(1000)

    def cycle(self, event):
        # gets called for every timer event
        # print(time.ctime())
        self.countdown.SetLabel("Countdown: {v1} Date: {v2}".format(v1=SECONDS_TO_WAIT - self.counter, v2=time.ctime()))
        self.countdown.Update()
        self.counter += 1
        if self.counter > SECONDS_TO_WAIT:
            self.quitprogram()

    def quitprogram(self):
        # stop the timer and close the frame
        self.timer.Stop()
        self.timer.Destroy()
        self.Destroy()
        # self.Close()
        # wx.Exit()


def main():
    app = wx.App()
    frame = MyForm(None, title="Timer-Test")
    frame.Show(True)
    app.SetTopWindow(frame)
    # wx.lib.inspection.InspectionTool().Show()
    app.MainLoop()
    if frame.counter > SECONDS_TO_WAIT:
        return 0
    else:
        return 1


if __name__ == '__main__':
    main()
